<?php
namespace Segaja;

use Twig;

class Template extends \stdClass
{
    /**
     * @var \Twig_TemplateWrapper
     */
    private $templateObject = null;

    /**
     * @var Twig\Environment
     */
    private $twig;

    /**
     * @var array
     */
    private $variables = [];

    /**
     * @param Twig\Environment $twig
     */
    public function __construct(Twig\Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @param string $key
     * @param mixed  $var
     */
    public function __set(string $key, $var)
    {
        $this->variables[$key] = $var;
    }

    /**
     * @param string $template
     *
     * @throws Twig\Error\LoaderError
     * @throws Twig\Error\RuntimeError
     * @throws Twig\Error\SyntaxError
     */
    public function createTemplate(string $template)
    {
        $this->templateObject = $this->twig->load($template . '.twig');
    }

    /**
     * @throws \Exception
     */
    public function display()
    {
        if (null === $this->templateObject) {
            throw new \Exception('You have to call `createTemplate()` first');
        }

        $this->templateObject->display($this->variables);
    }
}
