# Segaja/Template

## installation
```
composer require segaja/template
```

## Usage

```php
<?php
$twig = new \Twig\Environment(
    new \Twig\Loader\Filesystem('/path/to/templates/'),
    [ /* twig options array */ ]
);

$template = new \Segaja\Template($twig);

$template->createTemplate('welcomePage');

$template->variable1 = 'Hello world';

$template->display();
```

This example would create twig object that reads the templates from the `/path/to/templates/` directory.
The templates have to end with the `.twig` suffix.

In the end the template `welcomePage` is loaded, the `variable1` is assigned with the value "Hello world" and the template is displayed.
